package main

import (
	"context"
	"thisproject/activities"
)

func main() {
	activities.PrepareCoffee(context.TODO())
}
